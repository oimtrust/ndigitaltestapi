/// <reference types="Cypress"/>

const baseUrl = `${Cypress.env("apiBaseURL")}/v2/pokemon`

describe('REST API Test - Get Pokemon By Name of Pikachu', function() {

    beforeEach(() => {
        cy.request(`${baseUrl}/pikachu`).as('pokemon')
    });

    it('Validate Headers', () => {
        cy.get('@pokemon').its('headers').its('content-type').should('include', 'application/json; charset=utf-8')
    });

    it('Validate Status Code', () => {
        cy.get('@pokemon').its('status').should('equal', 200)
    });
    
    it('Validate Negative Status Code', () => {
        cy.request({
            method: 'GET',
            url: `${Cypress.env("apiBaseURL")}/v2/pokemon/pikapika`,
            failOnStatusCode: false,
        }).as('pikapika')
        cy.get('@pikapika').its('status').should('equal', 404)
    });

    it('Validate Content Include Pikachu', () => {
        cy.get('@pokemon').its('body').should('include', {name: 'pikachu'})
    });
});

describe('REST API Test - Get Pokemon By Name of Charizard', function() {

    beforeEach(() => {
        cy.request(`${baseUrl}/charizard`).as('pokemon')
    });

    it('Validate Headers', () => {
        cy.get('@pokemon').its('headers').its('content-type').should('include', 'application/json; charset=utf-8')
    });

    it('Validate Status Code', () => {
        cy.get('@pokemon').its('status').should('equal', 200)
    });

    it('Validate Content Include Charizard', () => {
        cy.get('@pokemon').its('body').should('include', {name: 'charizard'})
    });

    it('Validate available data of charizard', () => {
        cy.get('@pokemon').then((response) => {
            expect(response.body.base_experience).equal(response.body.base_experience)
        })
    });
});

describe('REST API Test - Get Pokemon By Name of Bulbasaur', function() {

    beforeEach(() => {
        cy.request(`${baseUrl}/bulbasaur`).as('pokemon')
    });

    it('Validate Headers', () => {
        cy.get('@pokemon').its('headers').its('content-type').should('include', 'application/json; charset=utf-8')
    });

    it('Validate Status Code', () => {
        cy.get('@pokemon').its('status').should('equal', 200)
    });

    it('Validate Content Include Bulbasaur', () => {
        cy.get('@pokemon').its('body').should('include', {name: 'bulbasaur'})
    });

    it('Validate available data of bulbasaur', () => {
        cy.get('@pokemon').then((response) => {
            expect(response.body.base_experience).equal(response.body.base_experience)
        })
    });
    
    it('Validate name bulbasaur: true', () => {
        cy.get('@pokemon').then((response) => {
            expect('bulbasaur').equal(response.body.name)
        })
    });
});

describe('REST API Test - Get Pokemon By Name of Mewtwo', function() {

    beforeEach(() => {
        cy.request(`${baseUrl}/mewtwo`).as('pokemon')
    });

    it('Validate Headers', () => {
        cy.get('@pokemon').its('headers').its('content-type').should('include', 'application/json; charset=utf-8')
    });

    it('Validate Status Code', () => {
        cy.get('@pokemon').its('status').should('equal', 200)
    });

    it('Validate Content Include Mewtwo', () => {
        cy.get('@pokemon').its('body').should('include', {name: 'mewtwo'})
    });

    it('Validate available data of mewtwo', () => {
        cy.get('@pokemon').then((response) => {
            expect(response.body.base_experience).equal(response.body.base_experience)
        })
    });
    
    it('Validate name mewtwo: true', () => {
        cy.get('@pokemon').then((response) => {
            expect('mewtwo').equal(response.body.name)
        })
    });
});
    