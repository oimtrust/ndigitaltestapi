/// <reference types="Cypress"/>

const baseUrl = `${Cypress.env("apiBaseURL")}/v2/pokemon/25`

describe('REST API Test - Get Pokemon By ID', function() {

    beforeEach(() => {
        cy.request(`${baseUrl}`).as('pokemon')
    });

    it('Validate Headers', () => {
        cy.get('@pokemon').its('headers').its('content-type').should('include', 'application/json; charset=utf-8')
    });

    it('Validate Status Code', () => {
        cy.get('@pokemon').its('status').should('equal', 200)
    });
    
    it('Validate Negative Status Code', () => {
        cy.request({
            method: 'GET',
            url: `${Cypress.env("apiBaseURL")}/v2/pokemon/25252525`,
            failOnStatusCode: false,
        }).as('pikapika')
        cy.get('@pikapika').its('status').should('equal', 404)
    });

    it('Validate Content', () => {
        cy.get('@pokemon').its('body').should('include', {name: 'pikachu'})
    });
});
    